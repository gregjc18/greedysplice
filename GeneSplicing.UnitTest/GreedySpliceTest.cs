﻿using GeneSplicing.Business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace GeneSplicing.UnitTest
{
    [TestFixture]
    public class GreedySpliceTest
    {
        [Test]
        public void Assemble_Empty()
        {
            List<string> segments = new List<string>();

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }

            Assert.Throws<Exception>(() => splicer.Assemble());
        }

        [Test]
        public void Assemble_Example()
        {
            List<string> segments = new List<string>
            {
                "all is well",
                "ell that en",
                "hat end",
                "t ends well"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }

            string result = splicer.Assemble();

            Assert.AreEqual("all is well that ends well", result);
        }

        [Test]
        public void Assemble_One()
        {
            List<string> segments = new List<string>
            {
                "Only one string"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }

            string result = splicer.Assemble();

            Assert.AreEqual("Only one string", result);
        }

        [Test]
        public void Assemble_Simple1()
        {
            List<string> segments = new List<string>
            {
                "ALL THE",
                "HE WORL",
                "WORLD IS",
                "LD IS A STAG",
                "GE"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            Assert.AreEqual("ALL THE WORLD IS A STAGE", result);
        }

        [Test]
        public void Assemble_Simple2()
        {
            List<string> segments = new List<string>
            {
                "IS A MAN",
                "MISERABLE PILE OF",
                "WHAT IS A",
                " OF SECRETS!",
                "N BUT A MISER",
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            Assert.AreEqual("WHAT IS A MAN BUT A MISERABLE PILE OF SECRETS!", result);
        }

        [Test]
        public void Assemble_Simple3()
        {
            List<string> segments = new List<string>
            {
                "TH",
                "HIS TE",
                "TEX",
                "EXT H",
                " HAS MAN",
                "ANY TEX",
                "EXT DUP",
                "DUPLICATES"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            Assert.AreEqual("THIS TEXT HAS MANY TEXT DUPLICATES", result);
        }

        [Test]
        public void Assemble_DuplicateSubstrings1()
        {
            List<string> segments = new List<string>
            {
                "THIS",
                "S MIGHT RUN FOREVER",
                "S MIGHT RUN FIVEVER"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            // Either string is correct
            Assert.Contains(result, new string[] {
                "THIS MIGHT RUN FOREVER",
                "THIS MIGHT RUN FIVEVER"
            });
        }

        [Test]
        public void Assemble_DuplicateSubstrings2()
        {
            List<string> segments = new List<string>
            {
                "HERE IS",
                "E IS SOM",
                "SOME TEXT HE",
                "HERE"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            Assert.AreEqual("HERE IS SOME TEXT HE", result);
        }

        [Test]
        public void Assemble_Unicode()
        {
            List<string> segments = new List<string>
            {
                "ȦȨȈȬȖ",
                "ȬȖȚɪ"
            };

            GreedySplice splicer = new GreedySplice();
            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble();

            Assert.AreEqual("ȦȨȈȬȖȚɪ", result);
        }

        [Test]
        public void Assemble_Stack1()
        {
            List<string> segments = new List<string>
            {
                "oh can't",
                "n't you he",
                "you hear",
                " hear me calli",
                "lling"
            };

            GreedySplice splicer = new GreedySplice();
            ICollection<(string result, string prefix, string suffix, int distance)> stackTrace = new List<(string result, string prefix, string suffix, int distance)>();

            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble(ref stackTrace);

            Assert.IsNotEmpty(stackTrace);
            Assert.AreEqual(4, stackTrace.Count);
            Assert.AreEqual("oh can't you hear me calling", result);
        }

        [Test]
        public void Assemble_Stack2()
        {
            List<string> segments = new List<string>
            {
                "this stack should",
                "stack should be really",
                "really obvio",
                "vious"
            };

            GreedySplice splicer = new GreedySplice();
            ICollection<(string result, string prefix, string suffix, int distance)> stackTrace = new List<(string result, string prefix, string suffix, int distance)>();

            foreach (string s in segments)
            {
                splicer.Insert(s);
            }
            string result = splicer.Assemble(ref stackTrace);

            Assert.IsNotEmpty(stackTrace);
            Assert.AreEqual(3, stackTrace.Count);
            Assert.AreEqual(("this stack should be really", "this stack should", "stack should be really", 12), stackTrace.ElementAt(0));
            Assert.AreEqual(("this stack should be really obvio", "this stack should be really", "really obvio", 6), stackTrace.ElementAt(1));
            Assert.AreEqual(("this stack should be really obvious", "this stack should be really obvio", "vious", 3), stackTrace.ElementAt(2));
            Assert.AreEqual("this stack should be really obvious", result);
        }

        [Test]
        public void Assemble_Bad1()
        {
            GreedySplice splicer = new GreedySplice();

            "m quaerat voluptatem.;pora incidunt ut labore et d;, consectetur, adipisci velit;olore magnam aliqua;idunt ut labore et dolore magn;uptatem.;i dolorem ipsum qu;iquam quaerat vol;psum quia dolor sit amet, consectetur, a;ia dolor sit amet, conse;squam est, qui do;Neque porro quisquam est, qu;aerat voluptatem.;m eius modi tem;Neque porro qui;, sed quia non numquam ei;lorem ipsum quia dolor sit amet;ctetur, adipisci velit, sed quia non numq;unt ut labore et dolore magnam aliquam qu;dipisci velit, sed quia non numqua;us modi tempora incid;Neque porro quisquam est, qui dolorem i;uam eius modi tem;pora inc;am al"
                .Split(';')
                .ToList()
                .ForEach(fragment => splicer.Insert(fragment));

            var actual = splicer.Assemble();

            Assert.AreEqual("Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.", actual);
        }
    }
}
