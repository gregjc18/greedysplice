﻿using GeneSplicing.Business;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GeneSplicing.UnitTest
{
    [TestFixture]
    class StringUtilTest
    {
        [Test]
        public void CalculateSuffixPrefixDistance_Simple1()
        {
            string a = "ALL THE WORLD";
            string b = "D IS A STAGE";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(1, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_EmptyAll()
        {
            string a = "";
            string b = "";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(0, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_EmptySuffix()
        {
            string a = "";
            string b = "abcde";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(0, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_EmptyPrefix()
        {
            string a = "";
            string b = "abcde";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(0, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_CommonSubstrings1()
        {
            string a = "ABCABCDEDEFGABCDE";
            string b = "ABCDEFG";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(5, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_CaseMismatch1()
        {
            string a = "ABCDEabcde";
            string b = "ABCDEFG";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(0, distance);
        }

        [Test]
        public void CalculateSuffixPrefixDistance_Contained1()
        {
            string a = "ABCABC";
            string b = "ABC";
            int distance = StringUtil.SuffixPrefixDistance(a, b);

            Assert.AreEqual(3, distance);
        }
    }
}
