﻿using System;
using System.Collections.Generic;
using GeneSplicing.Business;

namespace GeneSplicing.CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length == 1)
            {
                GreedySplice splice = GreedySplice.LoadFile(args[0]);
                ICollection<(string result, string prefix, string suffix, int distance)> stackTrace = new List<(string result, string prefix, string suffix, int distance)>();

                string result = splice.Assemble(ref stackTrace);

                // Print stacktrace
                foreach(var stack in stackTrace)
                {
                    Console.WriteLine($"Result={stack.result}, Pairs={stack.prefix},{stack.suffix}, Distance={stack.distance}");
                }

                Console.WriteLine(result);
            } else
            {
                Console.WriteLine(@"Usage: " +
                    "    program.exe <file.txt>");
            }
        }
    }
}
