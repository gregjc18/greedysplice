﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace GeneSplicing.Business
{
    public class GreedySplice
    {
        /// <summary>
        /// Loads substrings from a file into a new GreedyMatcher
        /// Each line is considered to be a unique substring
        /// </summary>
        /// <param name="filename">File whose contents are to be added to a new GreedySplice</param>
        /// <returns></returns>
        public static GreedySplice LoadFile(string filename)
        {
            GreedySplice splice = new GreedySplice();
            using (StreamReader reader = File.OpenText(filename))
            {
                string line;
                while((line = reader.ReadLine()) != null)
                {
                    splice.Insert(line);
                }
            }

            return splice;
        }

        private List<string> candidates = new List<string>();

        /// <summary>
        /// Insert a substring used during Assembly
        /// </summary>
        /// <param name="substring">substring to use during assembly</param>
        public void Insert(string substring)
        {
            candidates.Add(substring);
        }

        private (string result, string prefix, string suffix, int distance) AssembleOnce()
        {
            /**
             * We walk through each substring in the candidate list, comparing it to
             * every other substring and computing a distance.
             * 
             * The two unique substrings with the largest distanceScore (common suffix/prefix)
             * are combined into one superstring and removed from the candidate list.
             * The superstring is added to the candidate list for future matching
             * 
             * The last constructed superstring is always present in the last index of the array
             */

            string prefix = "";
            string suffix = "";
            int maxDistance = 0;

            for (int i = 0; i < candidates.Count; i++)
            {
                for (int j = 0; j < candidates.Count; j++)
                {
                    // Don't compare to self
                    if (i == j)
                    {
                        continue;
                    }

                    int distance = StringUtil.SuffixPrefixDistance(candidates[i], candidates[j]);
                    if (distance > maxDistance)
                    {
                        maxDistance = distance;
                        prefix = candidates[i];
                        suffix = candidates[j];
                    }
                }
            }

            if (maxDistance == 0)
            {
                return (null, null, null, 0);
            }

            string result = prefix.Remove(prefix.Length-maxDistance) + suffix;
            candidates.Remove(prefix);
            candidates.Remove(suffix);
            candidates.Add(result);

            return (result, prefix, suffix, maxDistance);
        }

        /// <summary>
        /// Assembles substrings into one 
        /// Records matches in stackTrace array
        /// </summary>
        /// <returns>Reconstructed string from substrings</returns>
        public string Assemble()
        {
            ICollection<(string result, string prefix, string suffix, int distance)> nullStackTrace = null;
            return Assemble(ref nullStackTrace);
        }

        /// <summary>
        /// Assembles substrings into one
        /// </summary>
        /// <param name="stackTrace">Optional stackTrace, recording results of submatches</param>
        /// <returns>Reconstructed string from substrings</returns>
        public string Assemble(ref ICollection<(string result, string prefix, string suffix, int distance)> stackTrace)
        {
            if (candidates.Count == 0)
            {
                throw new Exception("No substrings to reassemble!");
            }

            while (candidates.Count > 1)
            {
                var result = AssembleOnce();

                // Stop if no further matches possible
                if (result.result == null)
                {
                    break;
                }

                // logging/debugging
                if (stackTrace != null)
                {
                    stackTrace.Add(result);
                }
                Debug.WriteLine($"{result.result}, {result.prefix}, {result.suffix}, {result.distance}");

            }

            // Filter out substrings and return longest survivor
            return candidates.Where(str =>
            {
                foreach (string super in candidates)
                {
                    // Don't compare to ourselves
                    if (str == super)
                    {
                        continue;
                    }

                    if (super.Contains(str))
                    {
                        return false;
                    }
                }

                return true;
            }).OrderBy(s => -s.Length).First();
        }
    }
}
