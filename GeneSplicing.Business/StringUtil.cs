﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneSplicing.Business
{
    public class StringUtil
    {
        /// <summary>
        /// Computes the length of the common prefix/suffix of two strings
        /// </summary>
        /// <param name="prefix">String whose prefix index is to be calculated</param>
        /// <param name="suffix">String whose suffix is to be matched against</param>
        /// <returns>Number of characters in common prefix/suffix or -1</returns>
        public static int SuffixPrefixDistance(string prefix, string suffix)
        {
            /**
             * We iterate through each char in the prefix string starting at the beginning.
             * For each char in the prefix string, we iterate through the suffix string and 
             * increase a counter if both characters match.
             * 
             * If we manage to reach the end of the prefix string, we have found the longest suffix/prefix pair.
             * Further matches are possible, but they will always have a lower score
             */

            for(int i=0; i<prefix.Length; i++)
            {
                for(int j=0; j<suffix.Length; j++)
                {
                    if (prefix[j+i] != suffix[j])
                    {
                        break;
                    }

                    // Have we found the largest common prefix?
                    if (i+j+1 == prefix.Length)
                    {
                        return j+1;
                    }
                }
            }

            // No common prefix/suffix found
            return 0;
        }
    }
}
